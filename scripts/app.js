/* Javascript */
/* Funktion um alle Pop Ups erst zu schließen, bevor die anderen geöffnet werden. */
function closeAllPop(){
    var ups = document.getElementsByClassName('pop');
    for (var i=0; i< ups.length; i++){
        ups[i].classList.remove('showSome');
        ups[i].classList.add('hideSome');
    };
}

/* Funktion um das Impressums Popup einzublenden */
function showImpressum(){
    if($('#userPop').hasClass('hideSome')){
        closeAllPop();
        $('#impressumPop').removeClass('hideSome');
        $('#impressumPop').addClass('showSome');
    };
}

/* JQEURY */
$(document).ready(function() {

    //Erster Aufruf der Umfragen
    localDB.allDocs().then(function(result){
        if(result['total_rows'] != 0){
            showSurveys();
        };
    }).catch(function(err){
        console.log(err);
    })

    //Die Syncro direkt bei dem aufrufen der App mit aktuellen Daten starten.
    startSync();

/* Functions */
/* Function, um ein Element animiert per css zu drehen. Animation wird über einen Trick realisiert */
jQuery.fn.rotate = function(degrees) {
    $(this).animate({  borderSpacing: degrees }, {
    step: function(now,fx) {
      $(this).css('transform','rotate('+now+'deg)');
    },
    duration:'slow'
},'linear');
    return $(this);
};
/* ENDE */

/* Formular Auswertung für das hinzufügen einer Umfrage */
/* Wertet das Hinzufüge Formular aus, erstellt aus den Daten ein json und übergibt dies der add Funktion */
jQuery.fn.formEvaluate = function(event){

    //TEST KONSOLENAUSGABE
    //console.log(event);
//Grundfunktionalität.
    var object = {};
    var array = $(event.currentTarget).serializeArray();
    var correct = true;
//Überprüfen der Werte auf Gültigkeit.
  $.each(array, function(index, item) {
    if(item.name != "randomNumber"){
        if(item.value == ""){
            window.alert("Bitte jedes Feld ausfüllen. Erstes fehlendes Feld: "+item.name);
            correct = false;
            return false;
        }else if(item.name == "endDate"){
            var today = new Date().getTime();
            var given = 0;
            $.each(event.currentTarget, function(index, item){
                if(item.name == "endDate"){
                    given = item.valueAsNumber;
                };
            });
            if(today > given){
                window.alert("Das Datum muss in der Zukunft liegen");
                correct = false;
            };
        };
    };
  });

//Standardmäßiges Auswerten der Form.
//Wenn die Form gültig ist wird das Array in ein Object gewandelt. Später wird dieses in das passende json gewandelt.
if(correct){
  $.each(array, function(index, item) {
        if(item.name == "secure"){
            if(item.value == "Geheim"){
                object[item.name] = 1;
            }else if(item.value == "Öffentlich"){
                object[item.name] = 0;
            };
        }else{
            object[item.name] = item.value;
        };
  });

    if(object.hasOwnProperty('anwsers')){
        var anwsArr = object['anwsers'].split(';');
        var rdyAnwsArr = new Array();

        for(var i= 0; i < anwsArr.length; i++){

            var cache = {"anwser": anwsArr[i], "ergebnis": 0, "votes": []}

            rdyAnwsArr.push(cache);
        };
    };


    //Baut je nachdem um welche Form es sich handelt ein anderes json und nutzt eine andere Funktion zum übertragen der Daten.
    //Wenn die Form, die "add"-Form ist, dann ruf die "newSurvey" Funktion mit dem entsprechenden json auf.
    if(event.currentTarget.id == "add_form"){
//        console.log("Ich soll eine Umfrage hinzufügen");
        var currDate = new Date();
        var trans = {frage: object['question'],
                    secure: object['secure'],
                    createDate: currDate.getFullYear()+"-"+(currDate.getMonth()+1)+"-"+currDate.getDate(),
                    expired: object['endDate'],
                    autor: object['autor'],
                    anwsers: rdyAnwsArr,
                    state: 1};

//        console.log(trans);
        /* DATEN IN LOKALE DB EINTRAGEN */
        newSurvey(trans);
        return true;
    };

    //Wenn die Form, die user_form ist, dann erstelle ein lokales Dokument mit den Nutzerdaten in der localDB */
     if(event.currentTarget.id == "user_form"){
 //       console.log("Ich soll Nutzerdaten speichern");

        var rndchar = "";
        if(object['randomNumber'] == ""){
        var charArr = [48,65,97];
            //48 = Zahlen, 65 = Groß Buchstaben, 97 = klein Buchstaben
            //10 Zahlen möglich, 26 groß,klein Buchstaben möglich
        //Zufällige Zeichenfolge mit 8 Stellen erzeugen
        for(var t = 0; t < 9; t++){
            var rnd_t = Math.floor(Math.random() * 3);
            var rnd_z;
            if(rnd_t == 0){
                rnd_z = Math.floor(Math.random() * 10);
            }else{
                rnd_z = Math.floor(Math.random() * 26);
            };
            rndchar += String.fromCharCode(charArr[rnd_t] + rnd_z);
        }

//         console.log(rndchar);
            object['randomNumber'] = rndchar;
        }else{
            rndchar = object['randomNumber'];
        };

        var trans = {_id: "_local/userData",
                    _name: object['username'],
                    _rndKey: object['randomNumber'],
                    _specificID: md5(object['username']+rndchar)};

 //       console.log(trans);

        /* DOKUMENT IN DER LOKALEN DB EINTRAGEN */
         /* Wenn dies erfolgreich, dann auf alte Daten auf dem Server prüfen */
        localDB.put(trans).then(function(response){
 //           console.log("Nutzerdaten angelegt: "+response['ok']);
            window.alert("Deine zufällige Zeichenkette: "+rndchar);
            checkForOldData();
        }).catch(function(err){
            console.log(err);
        })
        return true;
    };

    //Wenn die Form, die incl_form ist, dann nimm die Umfrage ID entgegen, hol die Umfrage von der remoteDB und baue daraus das antworte Formular.
     if(event.currentTarget.id == "incl_form"){
 //       console.log("Ich soll eine Umfrage aus der remoteDB holen.");

        var getID = object['hashwert'];
        var getIDArr = [getID];
  //      console.log(getID);

        localDB.replicate.from(address,{doc_ids: getIDArr}).on('complete', function(info) {
  //           console.log(info);

            localDB.get(getID).then(function(doc){
//                console.log(doc);

                $(this).buildAnwserform(doc);

            }).catch(function(err){
                console.log(err);
            });

         }).on('error', function(err){
            console.log(err);
            window.alert("Internetverbindung nicht vorhanden. Bitte später erneut versuchen.");
         });

         return true;

    };

    //Wenn die Form, die abstimm_form ist, dann such die angehakten checkboxen, werte diese aus und füge deine Stimme der Umfrage hinzu.
    //Danach nochmal die startSync() Funktion ausführen
     if(event.currentTarget.id == "abstimm_form"){
//        console.log("Ich soll das Umfrageergebnis eintragen");
//         console.log(event);

        var getID = event.currentTarget.attributes.getNamedItem('document').value;
        var anzAntw = parseInt(event.currentTarget.attributes.getNamedItem('antworten').value);
 //           console.log(getID);
 //           console.log(anzAntw);

        var checkedAntw = new Array();
        var used = false;

         for(var i = 0; i < anzAntw; i++){
             $.each(event.currentTarget, function(index, item){
                if(item.name == ('a'+i)){
                    if(item.checked){
                        checkedAntw.push(item.defaultValue);
                    };
                };
            });
        };

 //       console.log(checkedAntw);

        localDB.get('_local/showedData').then(function(doc){
             if(doc['_idtoshow'].indexOf(getID) == -1)
             {
                doc['_idtoshow'].push(getID);
                    localDB.put(doc).then(function(response){
 //                       console.log(doc);
                        startSync();
                    }).catch(function(err){
                        console.log(err);
                });
            }
        }).then(function(){
                            localDB.get(getID).then(function(doc){
                                var document = doc;
 //                               console.log(document);
                                localDB.get('_local/userData').then(function(doc){

                                    for(var t = 0; t < document['anwsers'].length; t++){
                                        document['anwsers'][t].votes.forEach(function(entry){
                                            if(entry['obj'] == doc['_specificID']){
                                                used = true;
                                            };
                                        });
                                    };

                                    if(!used){
                                        for(var i = 0; i < anzAntw; i++){
                                            for(var z = 0; z < document['anwsers'].length; z++){
                                                if(document['anwsers'][z].anwser == checkedAntw[i]){
                                                    var cache = {"name" : object['deinname'], "obj" : doc['_specificID']};
                                                    document['anwsers'][z].ergebnis += 1;
                                                    document['anwsers'][z].votes.push(cache);
                                                };
                                            };
                                        };

 //                                       console.log(document);

                                        localDB.put(document).then(function(response){
//                                            console.log("Eintragen des neuen docs: "+response['ok']);
                                        }).catch(function(err){
                                            console.log(err);
                                        });
                                    }else{
                                        window.alert("Du hast bereits abgestimmt..");
                                    };

                                }).catch(function(err){
                                        console.log(err);
                                });

                            }).catch(function(err){
                                console.log(err);
                            });
        }).catch(function(err){
            console.log(err);
        });

        return true;
    };


};

};
/* ENDE */

/* Funktion um das abgesendete Formular zu leeren */
jQuery.fn.clearForm = function(){
    $(this).find('input:text, input:password, input:file, select, textarea').val('');
    $(this).find('[type=email], [type=date]').val('');
    $(this).find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
};
/* ENDE */

/* Funktion um das Popup für das Löschen einer Umfrage einzublenden */
/* Umfrage id wird aus dem DOM geladen und als attribut: surv im Button gespeichert.*/
/* Erst das Popup dynamisch füllen und anschließend Popup einblenden.*/
/* Popup Button "ok" führt das Löschen erst aus. */
jQuery.fn.showDelete = function(){
    var parentDiv = $(this).parent().parent();
    var getID = parentDiv.attr('id');

    var html = "";
    html += '<h3>Bestätigen</h3>';
    html += '<p>Damit wird diese Umfrage beendet und sämtliche Ergebnisse gehen verloren.</p>';
    html += '<p>Wirklich löschen?</p>';
    html += '<button class="deleteOk" type="button" name="ok" surv="'+getID+'"><span>Ok</span></button>';

    $('#dynamicConfig').html(html);

    closeAllPop();
    $('#dynamicPop').removeClass('hideSome');
    $('#dynamicPop').addClass('showSome');
}
/* ENDE */

/* Funktion um eine Umfrage zu löschen. */
/* Wird aufgerufen, wenn der ok Button der Bestätigung gedrückt wird */
/* Die ID der Umfrage wird aus dem Attribut "surv" des Buttons geholt */
jQuery.fn.deleteSurvey = function(){
    var getID = $(this).attr('surv');
    localDB.get(getID).then(function(doc){
        return localDB.remove(doc);
    }).then(function(result){
//        console.log("Löschen erfolgreich: "+result['ok']);
        unshowID(getID);
        closeAllPop();
        $('#dynamicConfig').html("");
    }).catch(function(err){
        console.log(err);
    });

}
/* ENDE */

/* Funktion um das Popup für das Ausblenden einer Umfrage einzublenden */
/* Umfrage id wird aus dem DOM geladen und als attribut: surv im Button gespeichert.*/
/* Erst das Popup dynamisch füllen und anschließend Popup einblenden.*/
/* Popup Button "ok" führt das Ausblenden erst aus. */
jQuery.fn.showHide = function(){
    var parentDiv = $(this).parent().parent();
    var getID = parentDiv.attr('id');

    var html = "";
    html += '<h3>Bestätigen</h3>';
    html += '<p>Damit wird diese Umfrage ausgeblendet.</p>';
    html += '<p>Wirklich ausblenden?</p>';
    html += '<button class="hideOK" type="button" name="ok" surv="'+getID+'"><span>Ok</span></button>';

    $('#dynamicConfig').html(html);

    closeAllPop();
    $('#dynamicPop').removeClass('hideSome');
    $('#dynamicPop').addClass('showSome');
}
/* ENDE */

/* Funktion um eine Umfrage auszublenden. */
/* Wird aufgerufen, wenn der ok Button der Bestätigung gedrückt wird */
/* Die ID der Umfrage wird aus dem Attribut "surv" des Buttons geholt */
jQuery.fn.hideSurvey = function(){
    var getID = $(this).attr('surv');
//    console.log("Umfrage: "+getID+" wurde ausgeblendet.");
    unshowID(getID);
    closeAllPop();
    $('#dynamicConfig').html("");
}
/* ENDE */

/* Funktion um das Popup für die Anzeige der Abstimmergebnisse anzuzeigen. */
/* Umfrage id wird aus dem DOM geladen und als attribut: surv im Button gespeichert.*/
/* Erst das Popup dynamisch füllen und anschließend Popup einblenden.*/
/* Popup Button "ok" schließt lediglich das Pop-Up. */
jQuery.fn.showResult = function(){
    var parentDiv = $(this).parent().parent();
    var getID = parentDiv.attr('id');

    var html = "";


    localDB.get(getID).then(function(doc){
        html += '<h3>Umfrageergebnisse</h3>';
        html += '<p>Hier kannst du die Ergebnisse deiner Umfrage einsehen.</p>';
        html += '<div class="resultTable">';
        html += '<div class="resultQuestDiv"><h3 class="resultQuesth3">'+doc['frage']+'</h3></div>';
        html += '<div class="resultHeading">Antwort</div>';
        html += '<div class="resultHeading">Dafür</div>';
        html += '<div class="resultHeading">Stimmen</div>';

        doc['anwsers'].forEach(function(entry){
            html += '<div class="resultCell">'+entry['anwser']+'</div>';
            html += '<div class="resultCell">'+entry['ergebnis']+'</div>';

            var names = "";
            if(entry['ergebnis'] > 0){
                var first = true;
                entry['votes'].forEach(function(entry){
                    if(first){
                        names += entry['name'];
                        first = false;
                    }else{
                        names += ', '+entry['name'];
                    };
                });
            };
            html += '<div class="resultCell">'+names+'</div>';
        });
        html += '</div>';
        html += '<button class="resultOK" type="button" name="ok"><span>Ok</span></button>';
        $('#dynamicConfig').html(html);

    }).catch(function(err){
        console.log(err);
    });

    closeAllPop();
    $('#dynamicPop').removeClass('hideSome');
    $('#dynamicPop').addClass('showSome');
}
/* ENDE */

/* EXTENDED IN MS4*/
/* Funktion um das Popup für die Anzeige der ID der Umfrage zum Teilen anzuzeigen. */
/* Umfrage id wird aus dem DOM geladen und als attribut: surv im Button gespeichert.*/
/* Erst das Popup dynamisch füllen und anschließend Popup einblenden.*/
/* Popup Button "ok" schließt lediglich das Pop-Up. */
jQuery.fn.showShare = function(){
    var parentDiv = $(this).parent().parent();
    var getID = parentDiv.attr('id');

    var html = "";

        html += '<h3>ID der Umfrage</h3>';
        html += '<p>Diese ID kannst du zum Teilen deiner Umfrage nutzen.</p>';
        html += '<form><input type="text" name="sharefield" value="'+getID+'"></form>'
        html += '<button class="shareOK" type="button" name="ok"><span>Ok</span></button>';
        $('#dynamicConfig').html(html);

    closeAllPop();
    $('#dynamicPop').removeClass('hideSome');
    $('#dynamicPop').addClass('showSome');
}
/* ENDE */

/* EXTENDED IN MS4*/
/* Funktion um das Popup für die Anzeige der Nutzerdaten. */
/* Erst das Popup dynamisch füllen und anschließend Popup einblenden.*/
/* Popup Button "ok" schließt lediglich das Pop-Up. */
jQuery.fn.showUser = function(){

    var html = "";

        localDB.get('_local/userData').then(function(doc){
//            console.log(doc);
            var usern = doc['_name'];
            var rkey = doc['_rndKey'];

            html += '<h3>Nutzerdaten</h3>';
            html += '<p>Dies sind deine aktuellen Nutzerdaten. Diese solltest du seperat sichern.</p>';
            html += '<form><input type="text" name="username" value="Nutzername: '+usern+'">';
            html += '<input type="text" name="rndZeichen" value="Zeichenkette: '+rkey+'"></form>';
            html += '<button class="userOK" type="button" name="ok"><span>Ok</span></button>';

            $('#dynamicConfig').html(html);

        }).catch(function(err){
            console.log(err);
        });

    closeAllPop();
    $('#dynamicPop').removeClass('hideSome');
    $('#dynamicPop').addClass('showSome');
}
/* ENDE */

/* Funktion, die die Antwort Form baut und einblendet */
jQuery.fn.buildAnwserform = function(doc){
    var document = doc;
    var htmlfinal = "";

    var antw = document['anwsers'];
//        console.log(antw);
    var anzAntw = antw.length;
 //       console.log(anzAntw);
    var count = 0;

    htmlfinal += '<h3>Bitte vergib deine Stimme.</h3>';
    htmlfinal += '<p>Hier kannst du abstimmen. Eine mehrfach Auswahl ist möglich. Mach einfach deine Haken und bestätige mit dem Button.</p>';
    htmlfinal += '<form class="form_popup" id="abstimm_form" document="'+document['_id']+'" antworten="'+anzAntw+'" method="post">';
    htmlfinal += '<input class="feld tt" type="text" name="deinname" placeholder="Dein Name"/>'

    antw.forEach(function(entry){
        htmlfinal += '<label class="feld tt"><input type="checkbox" name="a'+count+'" value="'+entry['anwser']+'"/>'+entry['anwser']+'</label>';
        count++;
    });

    htmlfinal += '<input class="feld" type="submit" value="Abstimmen" name="sub_V"/>'
    htmlfinal += '</form><div class="exit_but"><i class="icon-cancel exit_this"></i></div>';

    $('#anwsersPop').html(htmlfinal);

    closeAllPop();
    $('#anwsersPop').removeClass('hideSome');
    $('#anwsersPop').addClass('showSome');
}
/* ENDE */

/* Functions ENDE */

/* EXTENDED IN MS4*/
/* Share Buttons der Umfragen abhören */
$(document).on('click',"button.survey-share",function(){
    $(this).showShare();
});
    /* Auf den Ok Button des dynamischen Popups horchen */
    $(document).on('click',"button.shareOK",function(){
        closeAllPop();
        $('#dynamicConfig').html("");
    });

/* Lösch Buttons der Umfragen abhören */
$(document).on('click',"button.survey-delete",function(){
    $(this).showDelete();
});
    /* Auf den Ok Button des dynamischen Popups horchen */
    $(document).on('click',"button.deleteOk",function(){
        $(this).deleteSurvey();
    });

/* Hide Buttons der Umfragen abhören */
$(document).on('click',"button.survey-hide",function(){
    $(this).showHide();
});
    /* Auf den Ok Button des dynamischen Popups horchen */
    $(document).on('click',"button.hideOK",function(){
        $(this).hideSurvey();
    });

/* Result Buttons der Umfragen abhören */
$(document).on('click',"button.survey-detail",function(){
    $(this).showResult();
});
    /* Auf den Ok Button des dynamischen Popups horchen */
    $(document).on('click',"button.resultOK",function(){
        closeAllPop();
        $('#dynamicConfig').html("");
    });

/* Submit einer Form entgegen nehmen und auswerten */
$(document).on('submit','form.form_popup', function(e){
    e.preventDefault();
    var check = $(this).formEvaluate(e);
    if(check){
        $(this).clearForm();
        closeAllPop();
    };
});
/* ENDE */

/* Aktualisieren Button - Synchronisieren anstoßen */
$('#butRefresh').on('click',function(){
    if($('#userPop').hasClass('hideSome')){
        checkForOldData();
        startSync("ref");
    };
})
/* ENDE */

/* Menü Ausklappen + Menübutton drehen */
$('#butMenu').focusin(function () {
    if($('#userPop').hasClass('hideSome')){
        $(this).rotate(90);
        $('#butCreate').removeClass("closeNav");
        $('#butInsert').removeClass("closeNav");
        $('#butBackend').removeClass("closeNav");
        $('#butDelLocal').removeClass("closeNav");
        $('#butShowUser').removeClass("closeNav");
        $('.navigation').removeClass("closeNavBG");
        $('.navigation').addClass("openNavBG");
        $('#butCreate').addClass("openNav");
        $('#butInsert').addClass("openNav");
        $('#butBackend').addClass("openNav");
        $('#butDelLocal').addClass("openNav");
        $('#butShowUser').addClass("openNav");
        $('.navigation').attr("style","");
    };
});
/* ENDE */

/* Menü Einfahren + Menübutton zurück drehen */
$('#butMenu').focusout(function () {
        $(this).rotate(0);
        $('#butCreate').removeClass("openNav");
        $('#butInsert').removeClass("openNav");
        $('#butBackend').removeClass("openNav");
        $('#butDelLocal').removeClass("openNav");
        $('#butShowUser').removeClass("openNav");
        $('.navigation').removeClass("openNavBG");
        $('.navigation').addClass("closeNavBG");
        $('#butCreate').addClass("closeNav");
        $('#butInsert').addClass("closeNav");
        $('#butBackend').addClass("closeNav");
        $('#butDelLocal').addClass("closeNav");
        $('#butShowUser').addClass("closeNav");
    setTimeout(function(){
        $('.navigation').attr("style","display:none");
    },450);
});
/* ENDE */

/* Aktualisierungsbutton bei Betätigung drehen */
$('#butRefresh').on('click',function(){
    if($('#userPop').hasClass('hideSome')){
        $(this).rotate(180);
        $(this).rotate(0);
    };
})
/* ENDE */

/* Button Funktionen */
/* Umfrage erstellen -Popup einblenden */
$('#butCreate').on('click',function(){
    closeAllPop();
    $('.popupadd').removeClass('hideSome');
    $('.popupadd').addClass('showSome');
});
/* ENDE */

/* Umfrage hinzufügen -Popup einblenden */
$('#butInsert').on('click',function(){
    closeAllPop();
    $('.popupincl').removeClass('hideSome');
    $('.popupincl').addClass('showSome');
});
/* ENDE */

/* Popup Fenster schließen */
$(document).on('click','i.exit_this',function(){
    closeAllPop();
});
/* ENDE */

/* Result Fenster schließen */
$('#okButtonResult').on('click',function(){
    var popup = $(this).parent().parent();
    $(popup).removeClass('showSome');
    $(popup).addClass('hideSome');
});
/* ENDE */

/* Button um die lokale PouchDB zu löschen und eine neue anzulegen */
$('#butDelLocal').on('click',function(){
    localDB.destroy().then(function(response){
        console.log("DB Gelöscht: "+response['ok'])
    });
    localDB = new PouchDB('survDB');
});
/* ENDE */

/* EXTENDED IN MS4*/
/* Button um die aktuellen Nutzerdaten anzuzeiegn */
$(document).on('click','#butShowUser',function(){
    $(this).showUser();
});
    /* Auf den Ok Button des dynamischen Popups horchen */
    $(document).on('click',"button.userOK",function(){
        closeAllPop();
        $('#dynamicConfig').html("");
    });

    });
    //Document.ready ENDE

/* Tooltip einblenden - VORERST AUßER NUTZEN, NICHT WICHTIG*/
/*$('.tt').hover(function(){
    var hovertext = $(this).attr("hover");
    var html_code = '<span class=\"tooltip_pop\" id=\"tooltip\"></br>'+hovertext+'</span>';
    $(this).after(html_code);
},function(){
    $('#tooltip').remove();
});*/


/*      Datenbank Part          */

/* PouchDB einbinden */
/* Datenbank einbinden / erstellen */
//local
var localDB = new PouchDB('survDB');

/* Prüfen ob ein local Doc mit den Nutzerdaten vorhanden ist */
localDB.get('_local/userData').then(function(doc){
 //   console.log(doc);
}).catch(function(err){
 //   console.log("Keine Nutzerdaten gefunden, neu anlegen");
    closeAllPop();
    $('.popupUserdata').removeClass('hideSome');
    $('.popupUserdata').addClass('showSome');
});

/* Prüfen ob ein local Doc für die anzuzeigenden Umfragen existiert. Wenn nicht ,dann eins anlegen */
/* Innerhalb des Anlegens wird das Dokument direkt mit den bereits in der localDB liegenden Umfragen IDs gefüllt. */
localDB.get('_local/showedData').then(function(doc){
//    console.log(doc);
}).catch(function(err){
    console.log(err);
    localDB.put({
        _id: '_local/showedData',
        _idtoshow: new Array()
    }).then(function(){
        return localDB.get('_local/showedData');
    }).then(function(showedData){

        localDB.allDocs().then(function(result){
            var ids = new Array();
            result['rows'].forEach(function(entry){
                ids.push(entry['id']);
            });

            showedData['_idtoshow'] = ids;
            return showedData;

        }).then(function(showedData){
            localDB.put(showedData).then(function(result){
                localDB.get('_local/showedData').then(function(doc){
 //                   console.log(doc);
                });
            }).catch(function(err){
                console.log(err);
            });
        }).catch(function(err){
            console.log(err);
        });
    });
});

/* RESET showedData */
/*
 localDB.get('_local/showedData').then(function(doc){
     doc['_idtoshow'] = new Array();
 });*/

//Angezeigte Umfragen aktualisieren.

//remoteDB IP Variable
var address = 'https://surveys.pascalws.de:6984/survbase';

//remoteDB
/*
try{
var remoteDB = new PouchDB(address);
}catch(err){
    console.log(err);
};*/

 localDB.allDocs({include_docs: true}).then(function(result){
     console.log(result);
 });


/* Nach dem Anmelden überprüfen ob auf dem Server noch Umfragen von dieser Person liegen, wenn ja, dann diese den anzuzeigenden IDs
hinzufügen. */
/* Diese Funktion wird nach dem Anlegen der Nutzerdaten aus dem Popup heraus aufgerufen */
/* Innerhalb dieser Funktion, wird bei Erfolg eine Synchronisation angestoßen. Erfolg = Ids erfolgreich den anzuzeigenden IDs hinzugefügt. */
function checkForOldData(){

    var owner1 = "";
    localDB.get('_local/userData').then(function(doc){
        owner1 = doc['_specificID'];
    }).then(function(){
        localDB.replicate.from(address,{filter: 'fDesign/owners',query_params: {owner: owner1}}).on('complete', function(info) {
            console.log(info);

            var idToStore = new Array();
            localDB.allDocs({include_docs: true}).then(function(result){
//                console.log(result);
                result['rows'].forEach(function(entry){
                    var document = entry['doc'];
                        idToStore.push(document['_id']);
                });
            }).then(function(){
                localDB.get('_local/showedData').then(function(doc){

                    idToStore.forEach(function(entry){
                        if(doc['_idtoshow'].indexOf(entry) == -1){
                            doc['_idtoshow'].push(entry);
                        };
                    });

                    localDB.put(doc).then(function(response){
 //                       console.log(response);
                        showSurveys();
                    }).catch(function(err){
                        console.log(err);
                    });
                }).catch(function(err){
                  console.log(err);
                });
            }).catch(function(err){
            console.log(err);
            });
        }).on('error', function(err){
            console.log(err);
        });
    }).catch(function(err){
        console.log(err);
    });

}
/* ENDE */

/* Synchronisation - Einmalig bei aufruf + live mit retry anschließend */
//Es werden nur die angezeigten Umfragen synchronisiert. => traffic reduzieren. Nicht angezeigte Umfragen brauchen nicht aktualisiert werden.
//Ansonsten würde ich alle Dokumente der remoteDB bekommen.

function startSync(refr){
    localDB.get('_local/showedData').then(function(doc){
        var opts = { live: true, retry: true, doc_ids: doc['_idtoshow']};
//        console.log(opts);
        localDB.replicate.from(address,{doc_ids: doc['_idtoshow']}).on('complete', function(info) {

            showSurveys();
        }).on('error', function(err){
            if(refr == "ref"){
                window.alert("Synchronisierung fehlgeschlagen. Bitte später erneut versuchen.");
            }
            console.log(err);
//debug mobile            document.getElementById("debug").innerHTML = "Error bei der Erstsync"+err;
        });

            localDB.replicate.from(address, opts).on('change',function(info){
                console.log(info);
 //debug mobile               document.getElementById("debug").innerHTML = "change"+info['ok'];

                info['docs'].forEach(function(item){
                    if(item['_deleted'] == true){
                        unshowID(item['_id']);
                    };
                });

                showSurveys();
            }).on('denied',function(err){
                console.log(err);
//debug mobile                document.getElementById("debug").innerHTML = "Denied von remote: "+err;
            }).on('error',function(err){
                console.log(err);
//debug mobile                document.getElementById("debug").innerHTML = "Error von remote: "+err;
            });

            localDB.replicate.to(address, opts).on('change',function(info){
                console.log(info);
            }).on('denied',function(err){
                console.log(err);
//debug mobile                document.getElementById("debug").innerHTML = "Denied zu remote: "+err;
            }).on('error',function(err){
                console.log(err);
//debug mobile                document.getElementById("debug").innerHTML = "Error zu remote: "+err;
        });

    });
}

function unshowID(idDat){
    localDB.get('_local/showedData').then(function(doc){
        var oldids = doc['_idtoshow'];
        if(oldids.splice(oldids.indexOf(idDat),1) == idDat)
        {
            doc['_idtoshow'] = oldids;
            localDB.put(doc).catch(function(err){
                console.log(err);
            });
        };

        showSurveys();
    }).catch(function(err){
        console.log(err);
    });
}


/*
var opts = { live: true, retry: true };

remoteDB.replicate.to('localDB',idArr).on('complete', function(info) {
  remoteDB.sync('http://localhost:5984/survbase', opts)
    .on('change', onSyncChange)
    .on('paused', onSyncPaused)
    .on('error', onSyncError);
}).on('error', onSyncError);
*/
/* Testausgabe aller Dokumente in der Konsole */
/*
localDB.allDocs({
    include_docs: true,
    attachments: false
}).then(function(result){
    console.log(result);
}).catch(function(err){
    console.log(err);
});
*/
/* ENDE */

/* Testumfrage */
/*
var testUmfrage = {quest: "Welche Antwort ist die Richtige?",
                  secure: 0,
                  expired: "2018-05-10",
                  sendM: 0,
                  passp: "aligatoah3",
                  author: "Max",
                  mail: "maxweb@gmx.de",
                  answers: "Antwort A;Antwort B;Antwort C;Antwort D"};
*/


function newSurvey(umfrage){

    var finalDat = umfrage;
    var calcDate = new Date();
    finalDat['_id'] = md5(calcDate+finalDat['autor']);
    localDB.get('_local/userData').then(function(doc){
        finalDat['owner'] = doc['_specificID'];
    }).then(function(){
        localDB.put(finalDat).then(function(response){
 //           console.log("Success into Pouch");
 //           console.log("ID Des Objektes: "+response['id']);
//            console.log("Rev Des Objektes: "+response['rev']);

            localDB.get('_local/showedData').then(function(doc){
                doc['_idtoshow'].push(finalDat['_id']);
                localDB.put(doc).then(function(response){
                    showSurveys();
                    startSync();
                }).catch(function(err){
                    console.log(err);
                });
            }).catch(function(err){
                console.log(err);
            });

            localDB.get('_local/showedData').then(function(doc){
 //                       console.log(doc);
                    });



        }).catch(function(err){
           console.log("Error accured "+err);
        });
    }).catch(function(err){
        console.log(err);
    });
}

function showSurveys(){
    document.getElementById('loaderDiv').style.visibility = "visible";

    localDB.allDocs({include_docs: true}).then(function(result){

        var htmlout = "";

        localDB.get('_local/userData').then(function(doc){

            var userDoc = doc;

            localDB.get('_local/showedData').then(function(doc){

                result['rows'].forEach(function(entry){

                    var document = entry['doc'];

                    if(doc['_idtoshow'].indexOf(document['_id']) != -1)
                    {

                        var user = new Array();
                        document['anwsers'].forEach(function(entry){
                            entry['votes'].forEach(function(entry){
                                if(user.indexOf(entry['obj']) == -1){
                                    user.push(entry['obj']);
                                };
                            });
                        });
                        var votes = user.length;

                        var htmlcache = "";
                        htmlcache += '<div class="box" id="'+document['_id']+'">';
                        htmlcache += '<u class="survey-title"><h3>'+document['frage']+'</h3></u>';
                        htmlcache += '<h5 class="survey-date-start">Beginn: '+document['createDate']+'</h5>';
                        htmlcache += '<h5 class="survey-date-end">Ende: '+document['expired']+'</h5>';
                        htmlcache += '<h4 class="survey-author">'+document['autor']+'</h4>';
                        htmlcache += '<h5 class="survey-entry">Stimmen abgegeben: '+votes+'</h5>';
                        htmlcache += '<div class="survey-icons">';


                        if(document['owner'] == userDoc['_specificID']){
                            htmlcache += '<button type="button" class="SurveyButton-icon surveyButton survey-delete"><i class="icon-trash"></i></button>';
                            htmlcache += '<button type="button" class="SurveyButton-icon surveyButton survey-detail"><i class="icon-search"></i></button>';
                            htmlcache += '<button type="button" class="SurveyButton-icon surveyButton survey-share"><i class="icon-link"></i></button>';
                        }else {
                            htmlcache += '<button type="button" class="SurveyButton-icon surveyButton survey-hide"><i class="icon-cancel-circle"></i></button>';

                            if(document['secure'] == 0){
                                htmlcache += '<button type="button" class="SurveyButton-icon surveyButton survey-detail"><i class="icon-search"></i></button>';
                            };
                        };

                        htmlcache += '</div></div>';

                        htmlout += htmlcache;
                    }
                });

                if(htmlout != ""){
                    document.getElementById('helpBox').style.visibility = "hidden";
                };

                document.getElementById('inclPoint').innerHTML = htmlout;
                document.getElementById('loaderDiv').style.visibility = "hidden";
            }).catch(function(err){
                console.log(err)
                document.getElementById('loaderDiv').style.visibility = "hidden";
            });
        });

    }).catch(function(err){
        console.log(err);
        document.getElementById('loaderDiv').style.visibility = "hidden";
    })
}



/* md5 funktion aus dem Internet */
function md5(str) {
  var xl;

  var rotateLeft = function(lValue, iShiftBits) {
    return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
  };

  var addUnsigned = function(lX, lY) {
    var lX4, lY4, lX8, lY8, lResult;
    lX8 = (lX & 0x80000000);
    lY8 = (lY & 0x80000000);
    lX4 = (lX & 0x40000000);
    lY4 = (lY & 0x40000000);
    lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
    if (lX4 & lY4) {
      return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
    }
    if (lX4 | lY4) {
      if (lResult & 0x40000000) {
        return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
      } else {
        return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
      }
    } else {
      return (lResult ^ lX8 ^ lY8);
    }
  };

  var _F = function(x, y, z) {
    return (x & y) | ((~x) & z);
  };
  var _G = function(x, y, z) {
    return (x & z) | (y & (~z));
  };
  var _H = function(x, y, z) {
    return (x ^ y ^ z);
  };
  var _I = function(x, y, z) {
    return (y ^ (x | (~z)));
  };

  var _FF = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _GG = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _HH = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var _II = function(a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
  };

  var convertToWordArray = function(str) {
    var lWordCount;
    var lMessageLength = str.length;
    var lNumberOfWords_temp1 = lMessageLength + 8;
    var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
    var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
    var lWordArray = new Array(lNumberOfWords - 1);
    var lBytePosition = 0;
    var lByteCount = 0;
    while (lByteCount < lMessageLength) {
      lWordCount = (lByteCount - (lByteCount % 4)) / 4;
      lBytePosition = (lByteCount % 4) * 8;
      lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
      lByteCount++;
    }
    lWordCount = (lByteCount - (lByteCount % 4)) / 4;
    lBytePosition = (lByteCount % 4) * 8;
    lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
    lWordArray[lNumberOfWords - 2] = lMessageLength << 3;     lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
    return lWordArray;
  };

  var wordToHex = function(lValue) {
    var wordToHexValue = '',
      wordToHexValue_temp = '',
      lByte, lCount;
    for (lCount = 0; lCount <= 3; lCount++) {       lByte = (lValue >>> (lCount * 8)) & 255;
      wordToHexValue_temp = '0' + lByte.toString(16);
      wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
    }
    return wordToHexValue;
  };

  var utf8_encode = function(string) {
      string = (string+'').replace(/\r\n/g, "\n").replace(/\r/g, "\n");

      var utftext = "";
      var start, end;
      var stringl = 0;

      start = end = 0;
      stringl = string.length;
      for (var n = 0; n < stringl; n++) {
          var c1 = string.charCodeAt(n);
          var enc = null;

          if (c1 < 128) {               end++;           } else if((c1 > 127) && (c1 < 2048)) {               enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
          } else {
              enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
          }
          if (enc != null) {
              if (end > start) {
                  utftext += string.substring(start, end);
              }
              utftext += enc;
              start = end = n+1;
          }
      }

      if (end > start) {
          utftext += string.substring(start, string.length);
      }

      return utftext;
  }

  var x = [],
    k, AA, BB, CC, DD, a, b, c, d, S11 = 7,
    S12 = 12,
    S13 = 17,
    S14 = 22,
    S21 = 5,
    S22 = 9,
    S23 = 14,
    S24 = 20,
    S31 = 4,
    S32 = 11,
    S33 = 16,
    S34 = 23,
    S41 = 6,
    S42 = 10,
    S43 = 15,
    S44 = 21;

  str = utf8_encode(str);
  x = convertToWordArray(str);
  a = 0x67452301;
  b = 0xEFCDAB89;
  c = 0x98BADCFE;
  d = 0x10325476;

  xl = x.length;
  for (k = 0; k < xl; k += 16) {
    AA = a;
    BB = b;
    CC = c;
    DD = d;
    a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
    d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
    c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
    b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
    a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
    d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
    c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
    b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
    a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
    d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
    c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
    b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
    a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
    d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
    c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
    b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
    a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
    d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
    c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
    b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
    a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
    d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
    c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
    b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
    a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
    d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
    c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
    b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
    a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
    d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
    c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
    b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
    a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
    d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
    c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
    b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
    a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
    d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
    c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
    b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
    a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
    d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
    c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
    b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
    a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
    d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
    c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
    b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
    a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
    d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
    c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
    b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
    a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
    d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
    c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
    b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
    a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
    d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
    c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
    b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
    a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
    d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
    c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
    b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
    a = addUnsigned(a, AA);
    b = addUnsigned(b, BB);
    c = addUnsigned(c, CC);
    d = addUnsigned(d, DD);
  }

  var temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);

  return temp.toLowerCase();
}
