const CACHE_NAME = 'surv-cache-1';

const cacheUrls = [
    '/',
    '/index.html',
    '/pouchdb.min-6.4.1.js',
    '/jquery-3.2.1.min.js',
    '/images/launcher-icon-1x.png',
    '/images/launcher-icon-2x.png',
    '/images/launcher-icon-4x.png',
    '/images/loader.gif',
    '/styles/inline.css',
    '/styles/animation.css',
    '/styles/fontello.css',
    '/styles/fontello-codes.css',
    '/styles/fontello-embedded.css',
    '/styles/fontello-ie7.css',
    '/styles/fontello-ie7-codes.css',
    '/font/fontello.svg',
    '/scripts/app.js'
];

self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(cacheUrls);
      })
  );
});

this.addEventListener('fetch', function(event) {
      console.log(event.request);
    event.respondWith(
    caches.match(event.request).then(function(response) {
        if(response){
            console.log("Fetching with cached data");
            return response;
        }else{
            console.log("Fetching with online data");
            return fetch(event.request);
        };
    })
  );
});
