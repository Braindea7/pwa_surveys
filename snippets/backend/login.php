<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Backend - Survey</title>

        <link type="text/css" href="../../styles/inline_back.css" rel="stylesheet">
        <link type="text/css" href="../../styles/fontello.css" rel="stylesheet">

        <!--<link href="https://fonts.googleapis.com/css?family=Nunito:300,400" rel="stylesheet">-->
        <link type="text/css" href="../../styles/nunito_stylesheet.css" rel="stylesheet">
    </head>
    <body>
        <container class="container_login">

            <div class="loginFeld">
                <form id="loginForm" action="/backend.php" target="_self" method="post">
                    <input id="luser" type="text" name="user" placeholder="User" />
                    <input id="lpw" type="password" name="psw" placeholder="Password" />
                    <input id="lsub" type="submit" value="Login" />
                </form>
            </div>

            <div class="backField">
                <button id="backL" class="navigationButton" type="button" name="login_back" onclick="location.href='../../index.php'"><span>Back to Home</span></button>
            </div>
        </container>

        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <script type="text/javascript" src="../../jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="../../scripts/app.js"></script>
    </body>
</html>
