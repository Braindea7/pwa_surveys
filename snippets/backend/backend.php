<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Backend - Survey</title>

        <link type="text/css" href="../../styles/inline_back.css" rel="stylesheet">
        <link type="text/css" href="../../styles/fontello.css" rel="stylesheet">

        <!--<link href="https://fonts.googleapis.com/css?family=Nunito:300,400" rel="stylesheet">-->
        <link type="text/css" href="../../styles/nunito_stylesheet.css" rel="stylesheet">
    </head>
    <body>
        <container class="container">

        <header class="header">
            <h1 class="headerTitle">Backend-Survey</h1>
        </header>

        <main class="main">
            <sidebar class="sidebar">
                <h2 class="statisticTitle">Statistik</h2>
                <button id="butDeleteSurvey" class="navigationButton" type="button" name="Delete"><span>Survey löschen</span></button>
                <button id="butShowUser" class="navigationButton" type="button" name="show_User"><span>Nutzer anzeigen</span></button>
            </sidebar>

            <content class="content">
                <div class="statistic">
                    <div id="count_survey">
                        <h4 class="statistic_title">Anzahl Umfragen</h4>
                        <h4 id="counter_surv">10000</h4>
                    </div>
                    <div id="count_author">
                        <h4 class="statistic_title">Anzahl Autoren</h4>
                        <h4 id="counter_auth">1000</h4>
                    </div>
                    <div id="count_teilnehmer">
                        <h4 class="statistic_title">Anzahl Teilnehmer</h4>
                        <h4 id="counter_teiln">25000</h4>
                    </div>
                </div>
                <div class="config_window">
                </div>
            </content>
        </main>

        <footer class="footer">
            <button id="logout" class="navigationButton" type="button" name="logout"><span>Logout</span></button>
        </footer>

        <div class="loader">
        </div>

        </container>

        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <script type="text/javascript" src="../../jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="../../scripts/app.js"></script>
    </body>
</html>
